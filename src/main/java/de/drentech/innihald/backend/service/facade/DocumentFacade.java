package de.drentech.innihald.backend.service.facade;

import de.drentech.innihald.backend.domain.model.Document;
import de.drentech.innihald.backend.domain.repository.DocumentRepository;
import org.jboss.resteasy.core.ExceptionAdapter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class DocumentFacade {

    @Inject
    private DocumentRepository documentRepository;

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public List<Document> getAllDocuments() {
        this.logger.info("Get all documents");

        return this.documentRepository.listAll();
    }

    public Document getDocument(Long id) {
        this.logger.info("Get document with id " + id);

        return this.documentRepository.findById(id);
    }

    @Transactional
    public Document createDocument(Document document) {
        this.logger.info("Create document: " + document);

        document.persist();

        return document;
    }
}
