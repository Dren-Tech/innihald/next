package de.drentech.innihald.backend.controller;

import de.drentech.innihald.backend.domain.model.Document;
import de.drentech.innihald.backend.service.facade.DocumentFacade;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/document")
@Tag(name = "Document")
public class DocumentController {

    @Inject
    private DocumentFacade documents;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Document> listAction() {
        return this.documents.getAllDocuments();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Document detailAction(@PathParam Long id) {
        return this.documents.getDocument(id);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Document createAction(Document document) {
        return this.documents.createDocument(document);
    }
}
