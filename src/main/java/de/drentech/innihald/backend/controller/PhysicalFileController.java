package de.drentech.innihald.backend.controller;

import de.drentech.innihald.backend.domain.model.PhysicalFile;
import de.drentech.innihald.backend.domain.repository.PhysicalFileRepository;
import de.drentech.innihald.backend.service.facade.PhysicalFileFacade;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/physicalfile")
@Tag(name = "Document")
public class PhysicalFileController {

    @Inject
    private PhysicalFileFacade files;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<PhysicalFile> listAction() {
       return this.files.getAllFiles();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public PhysicalFile detailAction(@PathParam Long id) {
        return this.files.getPhysicalFile(id);
    }
}
