package de.drentech.innihald.backend.domain.repository;

import de.drentech.innihald.backend.domain.model.Document;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DocumentRepository implements PanacheRepository<Document> {

    public Document findById(Long id) {
        return find("id", id).firstResult();
    }
}
