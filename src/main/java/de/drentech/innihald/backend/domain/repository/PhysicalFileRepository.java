package de.drentech.innihald.backend.domain.repository;

import de.drentech.innihald.backend.domain.model.PhysicalFile;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PhysicalFileRepository implements PanacheRepository<PhysicalFile> {
}
