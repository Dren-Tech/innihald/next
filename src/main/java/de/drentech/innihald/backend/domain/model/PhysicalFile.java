package de.drentech.innihald.backend.domain.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class PhysicalFile extends PanacheEntity {

    private String filename;

    private String extension;

    public PhysicalFile() {
        this("", "");
    }

    public PhysicalFile(String filename, String extension) {
        this.filename = filename;
        this.extension = extension;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
