package de.drentech.innihald.backend.domain.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Document extends PanacheEntity {

    private String title;

    private String description;

    @OneToOne
    private PhysicalFile file;

    public Document(String title, String description, PhysicalFile file) {
        this.title = title;
        this.description = description;
        this.file = file;
    }

    public Document() {
        this("", "", new PhysicalFile());
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PhysicalFile getFile() {
        return file;
    }

    public void setFile(PhysicalFile file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "Document{" +
            "title='" + title + '\'' +
            ", description='" + description + '\'' +
            ", file=" + file +
            ", id=" + id +
            '}';
    }
}
