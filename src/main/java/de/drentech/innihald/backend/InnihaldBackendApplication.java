package de.drentech.innihald.backend;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.core.Application;

@OpenAPIDefinition(
    tags = {
        @Tag(name = "Document", description = "Operations related to documents."),
        @Tag(name = "Category", description = "Operations related to categories"),
        @Tag(name = "User", description = "Operations related to users.")
    },
    info = @Info(
        title = "Innihald API",
        version = "0.1.0",
        contact = @Contact(
            name = "Innihald Developer Support",
            url = "https://innihald.dren-tech.de",
            email = "devsupport@dren-tech.de"
        ),
        license = @License(
            name = "MIT",
            url = "https://opensource.org/licenses/MIT"
        )
    )
)
public class InnihaldBackendApplication extends Application {
}
